# Forensic challenge
This is a challange to find root shell backdoors.

## Requirements
Docker has to bee installed.

## Installation
- download all files
- extract
- open folder in shell
- execute `sudo docker build -t forensic .` to create docker container image with the name forensic.
- execute `sudo docker run -it forensic` to start the created container.

## Goal
A user left the computer unlocked with a open root shell.
Someone used the chance and installed backdoors for later use.
Find up to **13 backdoors**, that will grant you a root shell.

**Have fun and good luck.**

## Solutions
If you want to read the solutions read the Dockerfile. **CAUTION! SPOILERS!**
Try your best first.

